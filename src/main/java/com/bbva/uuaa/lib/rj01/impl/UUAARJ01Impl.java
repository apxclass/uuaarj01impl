package com.bbva.uuaa.lib.rj01.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.uuaa.dto.banco.CuentaDTO;

/**
 * The  interface UUAARJ01Impl class...
 */
public class UUAARJ01Impl extends UUAARJ01Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUAARJ01Impl.class);

	/**
	 * The execute method...
	 */
	@Override
	public boolean executeCreateAcount(CuentaDTO accountDTO) {
LOGGER.info("indica el insert de datos");

Map<String,Object>args=new HashMap<>();

args.put("DIVISA", "MXN");
args.put("CUENTA", "122");
args.put("TCUENTA", "CH");
args.put("IMPORTE", "12345");
int insert =this.jdbcUtils.update("INSERT_CUENTAS", args);

		return insert ==1;


	}
	
	@Override
	public List<CuentaDTO> excecuteGetAccount(){
		
		List<Map<String,Object>>cuentas=this.jdbcUtils.queryForList("SELECT_CUENTA",new HashMap<String,Object>());
		List<CuentaDTO>listCuentas=new ArrayList<>();
		
		if(cuentas.isEmpty()) {
			this.addAdvice("UUAA0004");
		}else {
			for(Map<String,Object>row:cuentas) {
				
			
			LOGGER.info("{}",row);
			CuentaDTO rowDTO=new CuentaDTO(); 
			
			rowDTO.setCdDivisa(row.get("CDDIVISA").toString());
			rowDTO.setCdTipoCuenta(row.get("CDTIPOCUENTA").toString());
			rowDTO.setImporte(Long.parseLong(row.get("NUCUENTA").toString()));
			rowDTO.setFhAlta(row.get("FHALTA").toString());
			listCuentas.add(rowDTO);
		}
			}
		return listCuentas;
	}
}
